## Levelling up

- https://twitter.com/b0rk/status/917252215752019968 (Julia Evans: How to be a wizard programmer)
- https://www.quora.com/How-can-I-become-a-very-good-programmer/answer/Brian-Knapp-1
- https://blog.ssanj.net/posts/2016-07-13-how-to-be-a-great-programmer.html
- https://www.youtube.com/watch?v=pQLG4U5A68k (Tai Lopez on Why Hard Work Isn't Enough)
- https://twitter.com/b0rk/status/980938694118002688
- https://medium.com/the-mission/15-things-you-need-to-be-successful-that-your-loser-ass-friends-dont-do-a3152dfd42a3
- http://chadfowler.com/2014/01/21/on-having-something-to-say.html
- https://medium.com/the-mission/19-harsh-truths-you-dont-want-to-hear-but-must-you-ll-be-10-times-better-for-it-41f4eb07b593
- https://artplusmarketing.com/30-things-about-life-everyone-should-learn-before-turning-30-eb58162e0b72
- https://medium.com/swlh/if-you-dont-know-what-you-want-this-is-for-you-6f7491ca2fb4
- https://medium.com/the-mission/your-big-break-is-never-coming-do-this-instead-8157786cfe8e
- https://twitter.com/b0rk/status/1004547854692683776?s=03 (Build Impossible Programs; better title is "How to build programs that look impossible to build")
- https://theascent.pub/3-years-from-now-if-you-dont-have-a-personal-brand-nobody-is-going-to-work-with-you-3fa53ea7b8e3
- https://www.quora.com/What-habit-makes-the-biggest-difference-in-your-life-with-the-least-effort/answer/Micha%C5%82-Stawicki
- https://jvns.ca/blog/2018/09/01/learning-skills-you-can-practice/
- https://www.youtube.com/watch?v=DKcHX_5dxdI (You don't need more than two years)
- https://www.youtube.com/watch?v=x-PhXOLW1X8 (Stop Wasting Energy on These 3 Undeniable Energy Wasters)
- https://www.youtube.com/watch?v=cqINcpbcZqs (7 Reasons Why Smart, Hardworking People Don't Become Successful)
- https://www.youtube.com/watch?v=u6LvZz6lBQ8 (How to Start with No Money)
- https://www.youtube.com/watch?v=aZEmzrRz0oU (Society's Problem with Patience and Why That's Problematic)
- https://www.howitactuallyworks.com/archives/future_you_masturbation.html (Future You Masturbation)
- http://www.azarask.in/blog/post/the-wrong-problem/ (You are solving the wrong problem)
- https://www.youtube.com/watch?v=87SAsfiA2gE (How to overcome your fear of failure)
- http://blog.samaltman.com/how-to-be-successful


## Productivity

- https://two-wrongs.com/pomohoro-combine-pomodoro-technique-hledger-timeclock-productivity
- https://blog.ssanj.net/posts/2015-02-22-time-poor-priority-rich.html
- https://blog.ssanj.net/posts/2015-03-15-dont-wait-for-moods.html
- https://zwischenzugs.com/2017/12/03/how-i-manage-my-time/
- https://blog.samaltman.com/productivity
- https://medium.com/personal-growth/the-akrasia-effect-why-we-dont-follow-through-on-what-we-set-out-to-do-and-what-to-do-about-it-7c8c47587897
- https://medium.com/darius-foroux/how-to-beat-procrastination-backed-by-science-969115e4e389
- http://matt.might.net/articles/college-tips/
- http://matt.might.net/articles/grad-student-resolutions/
- http://matt.might.net/articles/how-to-blog-as-an-academic/


## JFDI

- https://dev.to/damcosset/start-before-you-are-ready-393e
- https://florentcrivello.com/index.php/2018/05/20/nobody-cares/


## How to learn effectively

- https://www.quora.com/How-do-A+-students-in-college-study-for-their-exams/answer/Jon-Griffin-20
- https://betterhumans.coach.me/the-complete-guide-to-remembering-what-you-read-1ffb74b4e04a
- https://www.youtube.com/watch?v=4JhlfRAzVKY (罗辑思维 怎么样成为一个高手)


## Zen

- https://blog.ssanj.net/posts/2015-03-15-slowing-down.html


## Learning advice

- https://twitter.com/kmett/status/666306078640967680?s=03
- https://www.quora.com/What-is-one-piece-of-advice-you-would-like-to-give-to-students-as-an-educator/answer/Andrew-Ng


## Becoming a better person

- https://www.quora.com/By-whom-are-you-most-inspired-and-why/answer/Nelson-Wang-2
- https://blog.ssanj.net/posts/2016-08-28-how-to-get-job-in-any-economy.html
- https://medium.com/thrive-global/i-read-one-book-100-times-over-10-years-here-are-100-life-changing-lessons-i-learned-6b8499f89b


## Finding Mentors

- https://ryanholiday.net/finding-a-mentor/


## The art of reading

- https://ryanholiday.net/marginalia-the-anti-library-and-other-ways-to-master-the-lost-art-of-reading/


## Advice

- http://www.kalzumeus.com/2011/10/28/dont-call-yourself-a-programmer/
- http://thecodist.com/article/my-biggest-regret-as-a-programmer
- https://www.facebook.com/notes/ben-leong/work-life-balance-my-foot/10151518399452549 (Prof Ben: Work-Life Balance My Foot)
- https://news.ycombinator.com/item?id=13929735 (Yes I Still Want to Be Doing This at 56 (2012))
- https://www.quora.com/What-is-the-life-people-should-not-live/answer/Coal-Akida?share=175f36ea&srid=pXGZ
- https://www.quora.com/What-is-the-single-most-effective-piece-of-financial-advice-youve-ever-received/answer/Maverick-Lin?share=cf831b32&srid=pXGZ
- https://www.quora.com/Why-is-the-cost-of-living-in-Singapore-rising-so-fast-and-still-showing-no-sign-of-slowing/answer/Daniel-Tay-35
- http://paulgraham.com/hs.html
- https://www.quora.com/What-can-I-learn-know-right-now-in-10-minutes-that-will-be-useful-for-the-rest-of-my-life/answer/Jaco-Marais-4
- https://www.quora.com/What-should-I-start-doing-in-my-twenties-so-I-have-nothing-to-regret-at-40/answer/Yura-Yazlovytskyy
- https://www.quora.com/Has-your-friend-ever-given-you-advice-that-turned-out-to-be-meaningful/answer/Kanwal-Anuvind?share=6ef269e2&srid=pXGZ
- https://www.quora.com/What-did-you-do-in-your-20s-that-made-you-successful-later/answer/Ron-Rule
- https://www.quora.com/How-can-I-become-a-world-class-coder-in-under-three-years/answer/Bhaumik-Patel-7
- https://www.quora.com/What-is-the-one-thing-you-should-have-told-someone-before-its-too-late/answer/Justin-Miller-9
- https://www.quora.com/What-are-the-hardest-and-most-useful-skills-to-learn/answer/Gary-Vaynerchuk
- https://www.quora.com/What-are-5-ways-to-begin-improving-one%u2019s-self/answer/Elena-Ledoux
- https://www.quora.com/I-tried-harder-than-most-people-to-achieve-a-certain-goal-but-failed-How-do-I-cope-with-it/answer/Gordon-Miller-21
- https://www.quora.com/What-10-things-should-I-do-every-day-to-become-smarter/answer/Dylan-Woon-1?share=4638f5e1&srid=pXGZ
- https://qz.com/640112/the-career-advice-no-one-tells-you/
- https://www.quora.com/What-are-your-top-ten-happiness-habits/answer/Benito-Salazar-Jr
- https://pulptastic.com/fan-asked-mike-rowe-career-advice-response-got-something-everyone-read/
- https://www.quora.com/What-should-one-do-in-their-20s-to-avoid-regrets-in-their-30s-and-40s/answer/Wonwhee-Kim?fb_ref=Default&share=1&srid=pXGZ
- http://blog.samaltman.com/the-days-are-long-but-the-decades-are-short
- http://www.businessinsider.com/amazing-career-advice-for-college-grads-from-linkedins-billionaire-founder-2013-5?IR=T&
- https://www.quora.com/When-people-look-back-on-their-lives-what-are-common-regrets-they-have-Of-those-regrets-how-many-are-based-on-false-assumptions-or-premises/answer/James-Altucher
- https://www.quora.com/How-do-I-deal-with-the-fact-that-at-age-26-I-have-done-nothing-with-my-life-I-dont-think-I-have-a-passion-for-ANYTHING-I-do-enjoy-programming-but-I-have-zero-idea-on-what-to-do-as-a-personal-project-I%E2%80%99m-not-creative/answer/Megumi-Yamamoto-1
- https://www.quora.com/What-did-you-learn-after-working-for-a-decade-as-a-programmer/answer/Nat-Russo
- https://news.ycombinator.com/item?id=14873503 (Ask HN: How do you start giving tech talks?)
- https://www.quora.com/At-27-years-young-what-are-the-top-3-things-you-can-do-before-you-turn-30-to-be-able-to-say-at-50-I-have-made-it-in-life/answer/James-Altucher
- https://mtlynch.io/why-i-quit-google/
- https://medium.com/the-mission/why-most-people-will-remain-in-mediocrity-6c7e24c48d12
- https://jovicailic.org/2013/03/the-story-you-should-always-keep-in-mind/
- https://medium.com/the-mission/the-only-ten-things-that-have-given-me-success-39df9f5a916a
- https://medium.com/swlh/why-you-shouldnt-share-your-goals-be13fbe352d2
- https://www.quora.com/What-are-the-six-habits-that-will-change-your-life-forever/answer/Derek-Stark-4


## Insightful / Out of the box thinking

- https://www.samuelthomasdavies.com/elevator-waiting-times/


## Career Advice

- https://news.ycombinator.com/item?id=17043513 (Interviews vs. Auditions)
- https://www.quora.com/If-you-could-recommend-anything-to-me-what-would-it-be/answer/Yannick-Duchscher


## Inspirational

- https://www.quora.com/Why-do-I-feel-like-a-failure-How-can-I-stop-feeling-this-way/answer/James-Altucher
- https://www.quora.com/How-does-Quincy-Larson-make-money/answer/Quincy-Larson
- https://www.quora.com/What-are-some-mind-blowing-facts-that-sound-like-BS-but-are-actually-true/answer/Chethan-Kumar-1?srid=pXGZ&share=1
- https://collegeinfogeek.com/about/meet-the-author/my-impossible-list/
- https://www.quora.com/Do-Chinese-people-have-creativity/answer/Michelle-Zhou-28
- https://www.quora.com/Is-it-true-that-there-is-a-professor-at-UC-Berkeley-who-receives-a-standing-ovation-at-the-end-of-every-lecture-If-so-what-is-his-her-name-and-why/answer/Richard-Muller-3
- https://www.youtube.com/watch?v=0l4L00olExU (JRE with Jordan Peterson: what age should you have your life in order)
- https://www.quora.com/What-does-it-feel-like-to-go-from-being-wealthy-to-being-poor/answer/Michael-Aumock


## Negotiation

- https://medium.freecodecamp.org/how-not-to-bomb-your-offer-negotiation-c46bb9bc7dea


## Marketing yourself

- https://dev.to/exampro/700-web-developers-asked-me-to-give-them-linkedin-profile-feedback-and-these-are-my-5-top-tips-5382
