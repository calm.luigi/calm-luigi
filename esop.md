## ESOP / Stock options and pitfalls

- https://news.ycombinator.com/item?id=10705646 (Advice for people who can't exercise options due to large tax liabilities upon exercise)
- https://www.esofund.com/faq
- https://github.com/jlevy/og-equity-compensation
- https://blog.wealthfront.com/stock-options-14-crucial-questions/
- https://blog.wealthfront.com/an-employee-perspective-on-equity/
- https://www.codingvc.com/valuing-employee-options/
- https://news.ycombinator.com/item?id=2623777 (Mistakes you can't afford to make with stock options)
- https://news.ycombinator.com/item?id=10020063 (Implications of exercising startup options)
