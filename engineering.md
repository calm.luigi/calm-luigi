# About

## No category

- https://github.com/jhuangtw-dev/xg2xg
- https://news.ycombinator.com/item?id=17684502 (Modern SAT solvers: fast, neat and underused)
- https://engineering.squarespace.com/blog/2019/performance-tuning-postgres-within-our-tls-infrastructure
- https://blog.gruntwork.io/5-lessons-learned-from-writing-over-300-000-lines-of-infrastructure-code-36ba7fadeac1
- https://www.regular-expressions.info/catastrophic.html
- https://news.ycombinator.com/item?id=21289456 (Machine learning in UK financial services)
- https://news.ycombinator.com/item?id=20565425 (How I Became a Machine Learning Practitioner)
- https://medium.com/@njones_18523/chaos-engineering-traps-e3486c526059
- https://news.ycombinator.com/item?id=19969161 (Linux apps that run anywhere)
- https://news.ycombinator.com/item?id=19749069 (Setting up an ad-blocking VPN with wireguard and pihole)
- https://commandlinefanatic.com/cgi-bin/showarticle.cgi?article=art070
- https://medium.com/solo-io/api-gateways-are-going-through-an-identity-crisis-d1d833a313d7
- https://www.weave.works/blog/gitops-compliance-and-secure-cicd
- https://www.weave.works/blog/the-gitops-pipeline
- https://www.weave.works/blog/what-is-gitops-really
- https://aws.amazon.com/blogs/opensource/why-does-aws-open-source-the-firecracker-example/
- https://news.ycombinator.com/item?id=21472024 (Compiling at Compile Time)
- https://www.adaptivecapacitylabs.com/blog/2018/03/23/moving-past-shallow-incident-data/
- https://eng.lyft.com/announcing-envoy-c-l7-proxy-and-communication-bus-92520b6c8191
- https://blog.envoyproxy.io/service-mesh-data-plane-vs-control-plane-2774e720f7fc
- https://news.ycombinator.com/item?id=20462349 (Operating a large distributed system in a reliable way: practices I learned)
- https://stackshare.io/raygun/how-raygun-processes-millions-of-error-events-per-second
- https://www.quora.com/What-are-some-good-Python-projects-for-an-intermediate-programmer/answer/Yoshikii-Hamamura
- https://news.ycombinator.com/item?id=17522362 (Learn how to design large-scale systems)
- https://dnote.io/blog/writing-everything-i-learn-coding-for-a-month/
- https://www.quora.com/What-is-the-skill-that-made-you-a-productive-programmer/answer/Kurt-Guntheroth-1
- https://dev.to/sobolevn/i-am-a-mediocre-developer--30hn
- https://dev.to/nickjj/how-to-start-and-finish-any-web-app-project-3bad
- https://gist.github.com/rondy/af1dee1d28c02e9a225ae55da2674a6f (Notes on "The Effective Engineer", a book by Edmond Lau)
- https://dev.to/ivancrneto/delivering-in-time-is-a-matter-of-observing-the-details-i98
- https://www.quora.com/What-is-expected-of-a-junior-back-end-web-developer/answer/Jesse-Farmer
- https://www.quora.com/Why-is-C++-so-hard-How-can-I-improve-my-skills/answer/Gaetano-Checinski
- https://www.quora.com/What-are-the-best-secrets-of-great-programmers/answer/Guillermo-Schwarz?share=1&srid=pXGZ
- https://www.quora.com/What-does-the-Facebook-user-LIKE-database-look-like/answer/Diederik-van-der-Boor
- https://news.ycombinator.com/item?id=17360525
- https://fasterthanli.me/blog/2020/i-want-off-mr-golangs-wild-ride/


## Talks

- https://www.youtube.com/watch?v=ZSNg6KNzydQ (YOW! Lambda Jam 2018 - Jed Wesley Smith - Why "Names Don't Matter" Matters)
- https://twitter.com/JuniorDevSG/status/1007064655066378240
- https://twitter.com/engineersftw/status/923850563741106176 (Becoming a Tech Conference Speaker)
- https://www.youtube.com/watch?v=RUYPd7SjI-Y (Galois Tech Talk: Tree-Sitter: A New Parsing System for Programming Tools)


## Books

- http://a.co/42wFrXo (Linux Firewalls: Attack Detection and Response with iptables, psad, and fwsnort)
- http://reasonablypolymorphic.com/blog/thinking-with-types/ (Thinking with Types: Type-Level Programming in Haskell [Sandy Maguire])


## Resource Lists

- https://zwischenzugs.com/
- https://codewithoutrules.com
- http://chadfowler.com/posts/
