Alternative to OSCP course: https://www.elearnsecurity.com/certification/ecpptv2/

## Unclassified

- https://www.reddit.com/r/oscp/comments/a9e2yv/from_0_to_oscp_in_90days/ecndvkl/


## OSCP labs free alternatives

- https://www.vulnhub.com/
- https://www.hackthebox.eu/
- https://www.root-me.org/?lang=en
- http://overthewire.org/wargames/
- https://shellterlabs.com/en/


## OSCP enumeration guides

- https://gist.github.com/unfo/5ddc85671dcf39f877aaf5dce105fac3
- http://www.0daysecurity.com/penetration-testing/enumeration.html
- http://www.0daysecurity.com/pentest.html


## OSCP experiences

- https://medium.com/@m4lv0id/and-i-did-oscp-589babbfea19
- https://h4ck.co/oscp-journey-pwk-course-review/


## OSCP: good advice

- https://forums.offensive-security.com/showthread.php?9849-Are-there-any-teachers-tutors-for-PWK&p=47363#post47363
- https://forums.offensive-security.com/showthread.php?9849-Are-there-any-teachers-tutors-for-PWK&p=47365#post47365
- https://forums.offensive-security.com/showthread.php?2380-Course-Progression&p=9134#post9134


## OSCP guides

- https://sushant747.gitbooks.io/total-oscp-guide/


## Practice VMs

- https://www.abatchy.com/2017/02/oscp-like-vulnhub-vms.html (VulnHub VMs similar to OSCP)


## Linux Privilege Escalation

- https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation
- http://www.hackingarticles.in/editing-etc-passwd-file-for-privilege-escalation/
- https://www.hackingarticles.in/linux-privilege-escalation-using-suid-binaries/ (priv esc using well known programs that normally will not have suid bit on them)
- https://guif.re/linuxeop
- http://pentestmonkey.net/tools/audit/unix-privesc-check
- https://github.com/rebootuser/LinEnum
- https://github.com/InteliSecureLabs/Linux_Exploit_Suggester (last update was 5 years ago)
- https://github.com/sagishahar/lpeworkshop/blob/master/Lab%20Exercises%20Walkthrough%20-%20Linux.pdf


## Windows Privilege Escalation

- https://github.com/SecWiki/windows-kernel-exploits (very very comprehensive list of exploits)
- https://www.absolomb.com/2018-01-26-Windows-Privilege-Escalation-Guide/ (Windows counterpart to g0tmilk's post)
- https://pentestlab.blog/2017/04/24/windows-kernel-exploits/
- https://labs.mwrinfosecurity.com/assets/1089/original/Windows_Services_-_All_roads_lead_to_SYSTEM-1.1-oct15.pdf
- https://pentestlab.blog/2017/04/19/stored-credentials/
- http://www.greyhathacker.net/?p=738
- https://www.youtube.com/watch?v=PC_iMqiuIRQ (Level Up! Practical Windows Privilege Escalation - Andrew Smith)
- https://toshellandback.com/2015/11/24/ms-priv-esc/
- http://it-ovid.blogspot.com/2012/02/windows-privilege-escalation.html
- https://www.youtube.com/watch?v=kMG8IsCohHA (Encyclopedia of Windows Privilege Escalation)
- https://github.com/51x/WHP
- https://pentest.blog/windows-privilege-escalation-methods-for-pentesters/
- https://github.com/GDSSecurity/Windows-Exploit-Suggester
- https://github.com/jivoi/pentest/blob/master/exploit_win/win_local_exploits.md


## File uploads

- https://www.peerlyst.com/posts/bypassing-file-upload-filters-with-owasp-zap-request-editing-ben-berkowitz


## Walkthroughs

- https://www.gnucitizen.org/blog/coldfusion-directory-traversal-faq-cve-2010-2861/
- http://hacking-share.blogspot.com/2013/03/hacking-cold-fusion-servers-part-i.html


## Path traversal

- https://www.gracefulsecurity.com/path-traversal-cheat-sheet-windows/
- https://www.gracefulsecurity.com/path-traversal-cheat-sheet-linux/


## LFI

- https://www.idontplaydarts.com/2011/02/using-php-filter-for-local-file-inclusion/
- https://www.idontplaydarts.com/2011/03/php-remote-file-inclusion-command-shell-using-data-stream/
- https://diablohorn.com/2010/01/16/interesting-local-file-inclusion-method/
- https://www.exploit-db.com/papers/12886 (LFI - write shell using /proc/self/environ)
- https://www.hackingarticles.in/rce-with-lfi-and-ssh-log-poisoning/


## Reverse shell cheatsheets

- https://www.phillips321.co.uk/2012/02/05/reverse-shell-cheat-sheet/
- https://highon.coffee/blog/reverse-shell-cheat-sheet/


## Windows tools

- https://superuser.com/questions/322423/explain-the-output-of-icacls-exe-line-by-line-item-by-item


## Simple Buffer Overflow Exploit Development

- http://netsec.ws/?p=180


## General cheatsheets (generally a compilation of other people's overall experience)

- https://github.com/ucki/umpf/blob/master/UMPFV1-6-release.pdf
- https://jivoi.github.io/2015/07/01/pentest-tips-and-tricks/


## Command Injection

- http://repository.root-me.org/Administration/Unix/EN%20-%20Dangers%20of%20SUID%20Shell%20Scripts.pdf


## Pass the Hash

- https://myexploit.wordpress.com/control-keimpx-py-pass-the-hash/
- https://michael-eder.net/post/2018/native_rdp_pass_the_hash/


## suid programs

- http://repository.root-me.org/Administration/Unix/EN%20-%20SUID%20Privileged%20Programs.pdf


## Burp Free

- https://portswigger.net/burp/documentation/desktop/tools/proxy/options/invisible


## WebDAV

- https://code.blogs.iiidefix.net/posts/webdav-with-curl/


## TTY shells

- https://netsec.ws/?p=337


## Web Application attacks

- https://www.owasp.org/index.php/Cross_Site_Tracing


## Escape from restricted shell

- https://gtfobins.github.io/


### Passing the Hash

- https://www.offensive-security.com/metasploit-unleashed/psexec-pass-hash/
- https://www.christophertruncer.com/i-have-the-password-hashes-can-i-pass-them/


## Misconfiguration

- https://news.ycombinator.com/item?id=17370165 (Reverse Shell from an OpenVPN configuration file)


## Reverse shells

- https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/
- http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet


## Online brute force

- https://blog.g0tmi1k.com/dvwa/bruteforce-low/
