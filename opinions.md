Stuff that consists potentially mostly of opinions.

## Leadership

- https://hbr.org/2018/03/the-most-common-type-of-incompetent-leader
- http://randsinrepose.com/archives/the-new-manager-death-spiral/


## Engineering

- https://plus.google.com/110981030061712822816/posts/KaSKeg4vQtz (Steve Yegge: Notes from the Mystery Machine Bus)
- https://medium.freecodecamp.org/why-open-source-projects-sadly-favor-new-users-and-what-you-can-do-about-it-ba586038949e
- https://www.quora.com/What-are-the-biggest-myths-software-engineers-believe/answer/Tikhon-Jelvis
- https://www.quora.com/Do-programmers-that-write-bad-code-know-that-they-are-writing-bad-code/answer/Bob-Trower
- https://twitter.com/allenholub/status/1219016594036146177 (Words that DO NOT appear in the Agile Manifesto & Principles)


## Engineering Advice

- http://thecodist.com/article/how_many_people_does_it_take_to_write_software
- http://www.nomachetejuggling.com/2016/06/15/guidingprinciples-part1/
- http://www.nomachetejuggling.com/2016/06/20/guidingprinciples-part2/
- http://www.nomachetejuggling.com/2012/10/05/getting-real-work-done/
- https://www.quora.com/Does-working-at-Google-really-make-you-deliver-your-best-as-a-software-engineer/answer/Edmond-Lau
- https://www.quora.com/What-are-the-disadvantages-of-working-at-Google
- https://www.quora.com/Is-learning-to-code-becoming-harder/answer/Garry-Taylor-5
- https://news.ycombinator.com/item?id=16561956 (Ask HN: Does anybody still use jQuery? - More a discussion of reverting to simpler technologies)
- https://skillsmatter.com/skillscasts/14649-lightning-talk-why-i-no-longer-use-haskell

### Leading an engineering team / Engineering Leadership

- https://medium.com/@billjordan1/the-quiet-crisis-unfolding-in-software-development-cffbdafbf450

### Hiring (or anti-hiring) / Interviews

- https://channel9.msdn.com/Events/ALM-Summit/ALM-Summit-3/Technical-Interviewing-You-re-Doing-it-Wrong (Mitch Lacey, Jonathan Wanagel)
- http://www.daedtech.com/hiring-is-broken/
- https://blog.ssanj.net/posts/2015-04-09-hire-for-the-future.html
- https://blog.ssanj.net/posts/2015-10-21-hiring-the-cheapest-developers.html
- https://blog.hedges.net/2014/07/17/how-to-attract-great-people/
- https://danluu.com/programmer-moneyball/
- https://danluu.com/hiring-lemons/
- https://danluu.com/wat/
- https://www.codewithjason.com/make-organization-attractive-engineering-talent/
- https://www.netmeister.org/blog/interviewing-delusions.html

### Pragmatism in Engineering

- https://news.ycombinator.com/item?id=19576092 (You are not Google)

### Leading an engineering team / Engineering Leadership

- https://github.com/webflow/leadership/blob/master/tech_lead.md
- https://charity.wtf/2018/03/30/an-engineers-bill-of-rights-and-responsibilities/
- https://codeclimate.com/blog/the-role-of-being-technical-leadership/
- https://twitter.com/EricaJoy/status/991064975111176193 (If you are a manager of engineers, all these should come before you mess with code)


## Software Engineering as a career

- https://www.quora.com/How-do-Software-Engineers-cope-with-stress/answer/Tomasz-Smykowski


## The dark side of engineering

- https://hackernoon.com/12-signs-youre-working-in-a-feature-factory-44a5b938d6a2
- https://twitter.com/kelseyhightower/status/905134235614416896
- https://twitter.com/kelseyhightower/status/951816136714317825
- https://medium.com/@xevix/what-to-do-when-tech-jobs-go-bad-93e631a1bdc9
- https://www.quora.com/What-are-the-characteristics-of-a-bad-software-engineer/answer/Nauman-Rupani
- https://www.quora.com/What-do-you-dislike-about-the-current-state-of-programming-Note-that-I-am-not-asking-for-problems-in-specific-languages-like-Java-or-Haskell-but-more-about-what-you-dislike-about-our-current-tools-and-the-way-we-program/answer/Garry-Taylor-5
- https://blog.hedges.net/2014/04/08/4-symptoms-of-dysfunction-in-software-teams/
- https://www.quora.com/Why-did-you-stop-being-a-software-developer/answer/Ciaran-Irvine
- http://forums.hardwarezone.com.sg/111810431-post380.html
- https://news.ycombinator.com/item?id=18379050 (Anyone else find the new Gmail interface sluggish?)
- http://cryto.net/~joepie91/blog/2016/07/14/cloudflare-we-have-a-problem/
- https://blog.torproject.org/trouble-cloudflare


## Intriguing

- https://news.ycombinator.com/item?id=14313804 (A plan for open source software maintainers)


## Engineering Truths

- https://news.ycombinator.com/item?id=19576646 (You are not Google - why devs choose shiny tech)
- https://news.ycombinator.com/item?id=19576408 (You are not Google - why devs choose shiny tech part 2)
- https://twitter.com/kmett/status/1158003469007118336?s=09 (Why do some developers at strong companies like Google consider Agile development to be nonsense)


## Engineering Humor

- https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f
- https://gist.github.com/quchen/5280339
- https://twitter.com/nixcraft/status/865087685358006273?s=03 (How we see each other - devs, designers, pms, qa, sys admin)
- http://lifeofasoftwareengineer.tumblr.com/post/99660459858/refactoring
- https://twitter.com/unconed/status/678412252752519168?s=03
- https://www.flickr.com/photos/girliemac/sets/72157628409467125/ (HTTP status as cats)
- https://twitter.com/nixcraft/status/968004512370667520?s=03 (Unix program written 30 years ago vs. JS package written 30 months ago)
- https://www.quora.com/What-will-software-engineering-be-like-in-2022/answer/John-Byrd-2
- http://www.lizengland.com/blog/2014/04/the-door-problem/
- https://www.quora.com/As-a-software-engineer-developer-and-or-programmer-what-is-the-most-savage-comment-you-have-ever-seen-in-a-codebase/answer/Animesh-Jha-2
- http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-strip.jpg
- https://twitter.com/mengxilu/status/1119356923772489728 (Enterprise sales / customer success team helping clients launch POC)


## War stories

- https://www.quora.com/What-are-some-things-that-only-someone-who-has-been-programming-20-50-years-would-know/answer/Fred-Mitchell-5


## Programming anti-patterns

- https://en.wikipedia.org/wiki/Lava_flow_(programming)
- https://en.wikipedia.org/wiki/Anti-pattern


## Project Management

- https://blackboxofpm.com/ruthless-prioritization-e4256e3520a9


## JWT

- http://cryto.net/~joepie91/blog/2016/06/13/stop-using-jwt-for-sessions/


## Human Behavior / Psychology

- https://www.quora.com/What-is-the-easiest-way-to-catch-a-liar/answer/Stalin-Naufahu


## Stuff that should be common sense / common knowledge, but no

- https://www.quora.com/What-are-fine-dining-tips-for-first-timers/answer/Bjorn-Koch


## Black magic

- https://www.reddit.com/r/blackmagicfuckery/comments/8nidql/a_reallife_thneed/


## Arguments against starting own thing / working at small company

- https://news.ycombinator.com/item?id=17421586 (Ask HN: Would you rather work for Google or a smaller high growth company?)


## Money in software

- https://news.ycombinator.com/item?id=20359889


## Books

- http://a.co/cGrH5Pr (Rework [Jason Fried];)


## Bag

- https://news.ycombinator.com/item?id=14444914 (How to improve a legacy codebase)


## Very smart people

- http://www.t3x.org/lsi/preface.pdf

## Work culture

- https://rachelbythebay.com/w/2018/04/25/notequal/
- https://rachelbythebay.com/w/2012/02/11/moveon/
