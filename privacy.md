- https://github.com/tycrek/degoogle


## Tor / anonymity

- https://github.com/dgoulet/torsocks
- https://jovicailic.org/2012/06/tunneling-applications-with-tsocks-for-anonymity-using-tor/


## Privacy

- https://medium.freecodecamp.org/github-privacy-101-how-to-remove-personal-emails-from-your-public-repos-58347b06a508
