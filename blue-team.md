# About

Blue Team Security


## Tools

- https://github.com/ghantoos/lshell


## Certificate Authority


- https://security.stackexchange.com/questions/89319/creating-my-own-ca-for-an-intranet/89737#89737
- https://jamielinux.com/docs/openssl-certificate-authority/


## DNS Sinkhole

https://resources.infosecinstitute.com/dns-sinkhole/


## CI/CD Hardening

- https://www.slideshare.net/wickett/devsecops-and-the-cicd-pipeline


## General hardening

- https://www.linode.com/docs/security/securing-your-server/


## HTTP Headers

- https://dev.to/heroku/using-http-headers-to-secure-your-site-2no0
- https://www.smashingmagazine.com/2017/04/secure-web-app-http-headers/
- https://www.keycdn.com/blog/http-security-headers/


## HttpOnly cookies

- https://www.owasp.org/index.php/HttpOnly
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies


## HPKP (HTTP Public Key Pinning)

- https://scotthelme.co.uk/hpkp-http-public-key-pinning/


## CSP (Content Security Policy)

- https://scotthelme.co.uk/content-security-policy-an-introduction/
- https://www.html5rocks.com/en/tutorials/security/content-security-policy/
- https://developers.google.com/web/fundamentals/security/csp/


## CAA (Certificate Authority Authorization)

- https://scotthelme.co.uk/certificate-authority-authorization/


## OCSP Stapling

- https://scotthelme.co.uk/ocsp-expect-staple/


## Checklists

- https://medium.com/simple-security/web-developer-security-checklist-f2e4f43c9c56
