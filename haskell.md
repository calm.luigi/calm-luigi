# About

Haskell

## New links

- https://kseo.github.io/posts/2017-01-28-build-your-haskell-project-continuously.html
- https://teh.id.au/posts/2017/08/09/redirect-vulnerabilities/index.html
- https://twitter.com/jvanbruegge/status/1059827215695790081?s=03 (Haskell By Example on dev.to)
- https://news.ycombinator.com/item?id=18325933 (Writing a screencast video editor in Haskell)
- https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/guide-to-ghc-extensions
- https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/guide-to-ghc-extensions/basic-syntax-extensions
- https://blog.jle.im/entry/streaming-huffman-compression-in-haskell-part-1-trees.html
- https://haskell-lang.org/documentation
- https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#ghc-flag--XRank2Types
- http://www.haskellforall.com/2017/09/type-driven-strictness.html
- https://www.quora.com/Whats-a-good-scripting-functional-programming-language-for-quick-and-dirty-tasks-like-scraping-a-website-or-parsing-logs-that-is-fast-to-prototype-in-and-has-lots-of-easy-to-use-libraries/answer/Tikhon-Jelvis
- http://comonad.com/reader/2011/free-monads-for-less-3/
- http://wiki.haskell.org/Existential_type
- https://stackoverflow.com/questions/6872898/haskell-what-is-weak-head-normal-form
- http://reasonablypolymorphic.com/blog/recursion-schemes
- http://www.parsonsmatt.org/2015/09/24/recursion.html
- https://a-tour-of-go-in-haskell.syocy.net/en_US/index.html
- https://byorgey.wordpress.com/2010/07/06/typed-type-level-programming-in-haskell-part-ii-type-families/
- https://stackoverflow.com/questions/20870432/type-family-vs-data-family-in-brief
- https://leonmergen.com/
- https://news.ycombinator.com/item?id=1566704 ("very haskell ways" of writing directory traversals)
- https://stackoverflow.com/questions/13352205/what-are-free-monads
- https://stackoverflow.com/questions/47829007/why-do-i-need-so-much-memory-for-writert-state/47839335#47839335
- https://wiki.haskell.org/Random_shuffle
- https://hackage.haskell.org/package/monad-unlift
- https://lukepalmer.wordpress.com/2013/03/04/how-gadts-inhibit-abstraction/
- https://www.snoyman.com/blog/2018/02/conduitpocalypse (there are many links here, bookmark them)
- https://www.tweag.io/posts/2018-02-05-free-monads.html
- https://stackoverflow.com/questions/3276240/tools-for-analyzing-performance-of-a-haskell-program
- http://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf (Monads for functional programming)
- https://www.schoolofhaskell.com/school/starting-with-haskell/haskell-fast-hard
- https://artyom.me/haskell-ctf
- https://haskell-lang.org/library/async
- https://wiki.haskell.org/Performance/Monads
- https://twitter.com/HaskellLibHunt/status/977275577504706561?s=03 (CLI file manager using brick; Haskell)
- https://twitter.com/mattoflambda/status/977369704154017792?s=03 (ghcid command to rebuild project and run tests)
- https://twitter.com/HaskellLibHunt/status/978332510345035779?s=03 (Applied Category Theory - Online course)
- http://www.scs.stanford.edu/~dbg/readings/haskell-history.pdf
- https://www.fpcomplete.com/blog/2017/06/tale-of-two-brackets
- https://twitter.com/DiazCarrete/status/982917515662839808?s=03 (Why you should use Backpack)
- https://www.fpcomplete.com/blog/2018/05/pinpointing-deadlocks-in-haskell
- https://news.ycombinator.com/item?id=17044595 (Haskell's Missing Concurrency Basics (2016))
- https://twitter.com/welltyped/status/996274931171241984?s=03 (Objects with special collection routines in GHC's garbage collector)
- https://github.com/jayed/gogurl
- https://purelyfunctional.org/posts/2018-03-04-monadfix-lazy-strict-state.html
- https://www.stackage.org/package/unliftio
- https://twitter.com/kmett/status/993262362961547264?s=03
- https://twitter.com/FPComplete/status/994299066937888770?s=03
- https://twitter.com/mstk/status/996114822386343936?s=03 (A purely functional typed approach to Trainable Models -- 3 part series on Differentiable Programming in Haskell)
- https://wiki.haskell.org/Function_decoration_pattern
- https://twitter.com/mstk/status/993936521240571904?s=03
- https://twitter.com/EdskoDeVries/status/971688656275755008?s=03 (Object Oriented Programming in Haskell using open recursion and lenses)
- http://oleg.fi/gists/posts/2018-05-28-squash.html
- https://twitter.com/pigworker/status/1004305208816619522?s=03
- https://twitter.com/mstk/status/1008290541971259392?s=03
- http://thinkingwithtypes.com/
- https://news.ycombinator.com/item?id=17429132 (Smart constructors that cannot fail)
- https://twitter.com/typeclasses/status/1063670262242136064?s=19 (Functortown course by Monoid Mary)


## Read again

- https://lexi-lambda.github.io/blog/2017/04/28/lifts-for-free-making-mtl-typeclasses-derivable/
- https://markkarpov.com/post/free-monad-considered-harmful.html
- https://www.fpcomplete.com/blog/2017/06/readert-design-pattern
- https://www.fpcomplete.com/blog/2016/11/exceptions-best-practices-haskell
- https://lexi-lambda.github.io/blog/2018/02/10/an-opinionated-guide-to-haskell-in-2018/
- https://www.quora.com/How-harmful-is-Haskells-error-system-when-it-comes-to-pure-functions-in-the-real-world/answer/Tikhon-Jelvis
- http://dev.stephendiehl.com/hask/
- https://www.fpcomplete.com/blog/2017/06/understanding-resourcet
- https://lexi-lambda.github.io/blog/2016/06/12/four-months-with-haskell/
- https://www.quora.com/What-are-the-practical-application-areas-for-functional-programming-in-the-real-world/answer/Jacqueline-Homan


## Design Patterns

- https://wiki.haskell.org/Avoiding_IO
- http://www.haskellforall.com/2012/09/the-functor-design-pattern.html
- https://www.fpcomplete.com/blog/2018/01/weakly-typed-haskell
- http://www.parsonsmatt.org/2018/03/22/three_layer_haskell_cake.html (A way to design Haskell programs)


## Modelling effects (mainly mtl style typeclasses)

- https://www.reddit.com/r/haskell/comments/36e45c/mtl_is_not_a_monad_transformer_library/
- https://stackoverflow.com/questions/2769487/mtl-transformers-monads-fd-monadlib-and-the-paradox-of-choice?rq=1
- https://stackoverflow.com/questions/48392810/limiting-effects-like-with-freer-using-mtl-style


## Lenses

- https://artyom.me/lens-over-tea-1
- https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/basic-lensing
- https://blog.jakuba.net/2014/07/14/lens-tutorial-introduction-part-1.html
- https://blog.jakuba.net/2014/08/06/lens-tutorial-stab-traversal-part-2.html
- https://en.wikibooks.org/wiki/Haskell/Lenses_and_functional_references
- http://lens.github.io/tutorial.html
- https://lens.github.io/
- https://en.wikibooks.org/wiki/Haskell/Lenses_and_functional_references


## Performance

- https://twitter.com/StephenPiment/status/942988464647127040 (The Haskell Performance Checklist)
- https://twitter.com/mattoflambda/status/942831984564248576 (Haskell performance debugging)


## Beginner tutorials

- http://howistart.org/posts/haskell/1/
- https://www.fpcomplete.com/blog/2016/08/bitrot-free-scripts
- https://www.schoolofhaskell.com/school/starting-with-haskell/basics-of-haskell


## JSON Parsing

- http://hao.codes/lenses-heart-json.html


## Libraries

- https://haskell-lang.org/libraries
- https://two-wrongs.com/haskell-time-library-tutorial
- https://github.com/Gabriel439/Haskell-Turtle-Library
- https://github.com/jaspervdj/patat
- https://hackage.haskell.org/package/http-client-0.5.8
- https://hackage.haskell.org/package/http-conduit
- https://hackage.haskell.org/package/optional-args-1.0.2/docs/Data-Optional.html
- https://hackage.haskell.org/package/managed-1.0.6/docs/Control-Monad-Managed.html


## Monad Transformers

- https://www.reddit.com/r/haskellquestions/comments/3w9knl/resources_to_understand_monad_transformers/
- http://catamorph.de/publications/2004-10-01-monad-transformers.html
- https://ocharles.org.uk/blog/posts/2016-01-26-transformers-free-monads-mtl-laws.html


## Conduit

- https://docs.google.com/presentation/d/1RBefOCZ7AKOo4f1yiF4mtKPAT3l5vY9ky2SR02O4Vvg/edit#slide=id.p16
- https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/conduit-overview
- https://haskell-lang.org/library/conduit
- https://github.com/snoyberg/conduit (Stopped at `Primitives` section)


## Exceptions

- https://www.fpcomplete.com/blog/2018/04/async-exception-handling-haskell


## Dependency injection

- https://github.com/toddaaro/haskell-di
- https://www.reddit.com/r/haskell/comments/6fn09r/dependency_injection_in_haskellfeedback_wanted/


## Testing

- https://jaspervdj.be/posts/2015-03-13-practical-testing-in-haskell.html
- https://www.reddit.com/r/haskell/comments/5mrk20/haskell_testing_frameworks_what_do_you_use/
- https://stackoverflow.com/questions/17615138/haskell-quickcheck-to-generate-and-test-rose-trees


## Tools

- https://github.com/serokell/importify
- https://twitter.com/HaskellLibHunt/status/934695048582533120 (neovim-ghci: a fast-feedback interactive Haskell editing experience)
- https://twitter.com/github_haskell/status/942527852565671936 (dumbwaiter - Extensible HTTP Web server configured entirely by a yaml file)


## Style / indentation

- http://chrisdone.com/posts/hindent
- https://github.com/tibbe/haskell-style-guide/blob/master/haskell-style.md
- https://github.com/jaspervdj/stylish-haskell/blob/master/README.markdown


## Hakyll

- https://www.blaenkdenum.com/posts/post-feed-in-hakyll/
- http://mattwetmore.me/posts/hakyll-custom-writer.html


## Haskell code to study

- https://github.com/themoritz/diener
- https://github.com/metabrainz/CAA-indexer/blob/4fb2f260f1b5dd66d3dcaef1a188818a89705002/src/Main.hs
- https://github.com/haskell-servant/example-servant-minimal
- https://github.com/lpaste/lpaste


## Harder stuff

- https://news.ycombinator.com/item?id=15440108 (Functor-Oriented Programming responses on HN)
- https://lobste.rs/s/walsxp/functor_oriented_programming (Functor-Oriented Programming responses on lobsters)
- https://www.reddit.com/r/haskell/comments/75fo8k/functor_oriented_programming/ (Functor-Oriented Programming responses on Reddit)
- https://github.com/lexi-lambda/freer-simple
- https://www.reddit.com/r/haskellquestions/comments/7jssi5/what_are_free_monoids/
- https://twitter.com/GabrielG439/status/942233323077754881 (Apparently an import sanity check that Dhall inherited from Morte accidentally doubles as a security feature)
- https://twitter.com/BartoszMilewski/status/942786660512751616 (Category Theory Crash Course from Lyon @ScalaIO_FR)
- https://cstheory.stackexchange.com/questions/1539/whats-new-in-purely-functional-data-structures-since-okasaki


## Papers

- https://ncatlab.org/nlab/files/WadlerMonads.pdf (Phil Wadler: Comprehending Monads)
- http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.144.2237&rep=rep1&type=pdf (Lazy Functional State Threads)


## Needs time

- http://haskell.cs.yale.edu/wp-content/uploads/2011/01/cs.pdf
- https://www.haskell.org/communities/11-2017/html/report.html (Haskell Communities and Activities Report: 33rd edition - Nov 2017)


## Can translate to Haskell

- https://github.com/nomad-software/meme
- https://github.com/arc90/git-sweep


## Study

- https://github.com/lexi-lambda/mtl-style-example
- https://twitter.com/vamchale/status/1108488627003240448?s=03 (libarchive)


## Architecture

- http://abailly.github.io/posts/cm-arch-design.html
- http://abailly.github.io/posts/cm-infra-1.html
- http://abailly.github.io/posts/cm-infra-2.html


## Stack

- https://haskell-lang.org/tutorial/stack-script
- https://haskell-lang.org/tutorial/stack-build


## Downsides

- https://www.reddit.com/r/haskell/comments/4f47ou/why_does_haskell_in_your_opinion_suck/


## Category Theory

- https://www.quora.com/What-is-an-endofunctor/answer/Tikhon-Jelvis
- https://www.benjamin.pizza/posts/2017-12-15-functor-functors.html


## GADTs

- https://en.wikibooks.org/wiki/Haskell/GADT


## Functors

- https://stackoverflow.com/questions/13134825/how-do-functors-work-in-haskell?rq=1


## Applicative Functors

- https://stackoverflow.com/questions/6570779/why-should-i-use-applicative-functors-in-functional-programming
- https://stackoverflow.com/a/2395956/732396 (What is lifting?)
- https://stackoverflow.com/questions/29453915/composing-monads-v-applicative-functors
- https://stackoverflow.com/questions/7040844/applicatives-compose-monads-dont


## Monads

- https://stackoverflow.com/questions/31652475/defining-a-new-monad-in-haskell-raises-no-instance-for-applicative
- https://stackoverflow.com/a/28141248 (Why do we need monads?)
- https://stackoverflow.com/a/17412402/732396 (answer to `What advantage does Monad give us over an Applicative?`)
- https://mmhaskell.com/blog/2017/3/6/making-sense-of-multiple-monads (Monad Transformers; learnt how to chain MaybeT. Makes Monad Transformers a lot more interesting)
- https://stackoverflow.com/a/12620418/732396  (Example showing IO doesn't satisfy monad laws; Nah, not without seq)
- https://softwareengineering.stackexchange.com/a/163840  (Critique of IO monad viewed as State monad operating on world)
- https://www.quora.com/Why-do-we-need-monad/answer/Quildreen-Motta


## Reader Monad

- https://www.reddit.com/r/haskell/comments/2acj3w/threading_argument_vs_reader_monad/
- https://www.reddit.com/r/haskell/comments/4c9en3/when_to_prefer_a_reader_monad_over_a_function/


## Writer Monad

- https://kseo.github.io//posts/2017-01-21-writer-monad.html


## State Monad

- https://blog.jle.im/entry/unique-sample-drawing-searches-with-list-and-statet.html
- https://blog.jle.im/entry/unique-sample-drawing-searches-with-list-and-statet.html


## ST monad

- https://stackoverflow.com/questions/34494893/how-to-understand-the-state-type-in-haskells-st-monad
- https://stackoverflow.com/questions/8197032/starray-documentation-for-newbies-and-state-st-related-questions
- https://stackoverflow.com/questions/12468622/how-does-the-st-monad-work


## Continuation Monad

- http://www.haskellforall.com/2012/12/the-continuation-monad.html (The Continuation Monad; quite mindblowing)
- https://wiki.haskell.org/MonadCont_under_the_hood
- https://www.quora.com/What-is-continuation-passing-style-in-functional-programming/answer/Tikhon-Jelvis
- https://en.wikibooks.org/wiki/Haskell/Continuation_passing_style


## Reverse State Monad

- https://lukepalmer.wordpress.com/2008/08/10/mindfuck-the-reverse-state-monad/
- https://www.quora.com/What-are-some-crazy-things-one-can-do-with-monads-in-Haskell/answer/Tikhon-Jelvis
- https://news.ycombinator.com/item?id=4349369 (Tikhon Jelvis' explanation)


## PrimMonad, PrimState

- https://stackoverflow.com/a/17378865


## Monad Transformers

- http://blog.sigfpe.com/2006/05/grok-haskell-monad-transformers.html
- https://en.wikibooks.org/wiki/Haskell/Monad_transformers
- https://github.com/kqr/gists/blob/master/articles/gentle-introduction-monad-transformers.md
- https://wiki.haskell.org/Simple_StateT_use  (StateT monad examples)
- https://stackoverflow.com/questions/11792275/is-access-to-the-internal-structure-of-a-monad-required-for-a-monad-transformer/11794441


## Modelling effects

- https://making.pusher.com/3-approaches-to-monadic-api-design-in-haskell/
- https://markkarpov.com/post/free-monad-considered-harmful.html
- https://hackage.haskell.org/package/freer-simple-1.0.1.1/docs/Control-Monad-Freer.html
- https://lexi-lambda.github.io/blog/2018/02/10/an-opinionated-guide-to-haskell-in-2018/ (Search for the `Having an effect` section)
- https://github.com/lexi-lambda/mtl-style-example
- https://mail.haskell.org/pipermail/haskell-cafe/2018-September/129992.html


## Arrows

- http://newartisans.com/2012/10/arrows-are-simpler-than-they-appear/


## Functional dependency

- https://stackoverflow.com/questions/497449/what-does-a-pipe-in-a-class-definition-mean
- https://stackoverflow.com/questions/2675655/whats-the-for-in-a-haskell-class-definition
- https://wiki.haskell.org/Functional_dependencies


## RankNTypes

- https://stackoverflow.com/a/12031900 (concise answer to `What is the purpose of Rank2Types?`)
- https://stackoverflow.com/questions/12031878/what-is-the-purpose-of-rank2types/
- https://medium.com/@paul_meng/what-is-rank-n-types-in-haskell-ce30797dcc67


## Data Structures

- http://h2.jaguarpaw.co.uk/posts/demystifying-dlist/


## State of Haskell

- https://github.com/Gabriel439/post-rfc/blob/master/sotu.md (State of the Haskell ecosystem)


## Beginner Advice

- http://argumatronic.com/noobs.html
- http://web.archive.org/web/20100416040111/http://www.alpheccar.org/en/posts/show/67  (Haskell study plan by alpheccar to a friend)


## Some small tricks

- https://stackoverflow.com/a/2327801/732396 (Force Floats to have 2 decimals)
- https://stackoverflow.com/q/38727850 (How to deal with an IO (Maybe (IO (Maybe t))) type?)
- https://stackoverflow.com/q/20293006/732396 (How to use Maybe Monad inside another Monad?)
- https://stackoverflow.com/a/13408074/732396 (Don't echo on stdout what the user types via stdin)
- https://stackoverflow.com/a/38734638 (representing "end with" in Parsec)


## Different skill tiers in Haskell

- https://stackoverflow.com/a/1016986/732396 (Getting started with Haskell)
- http://lambdaconf.us/downloads/documents/lambdaconf_slfp.pdf (Standardized ladder of functional programming - a poster)


## Dependency Injection

- https://stackoverflow.com/q/14327327  (Dependency injection in Haskell: solving the task idiomatically)
- https://stackoverflow.com/a/23872461  (an answer with working code to `Dependency injection in Haskell: solving the task idiomatically`)
- https://twitter.com/danclien/status/972212552313778177?s=03 (Twitter poll on preferred dependency injection methods)


## Void / absurd

- https://stackoverflow.com/questions/14131856/whats-the-absurd-function-in-data-void-useful-for
- https://stackoverflow.com/questions/38556531/how-is-data-void-absurd-different-from-⊥
- https://www.fpcomplete.com/blog/2017/07/to-void-or-to-void


## Exceptions

- https://haskell-lang.org/library/safe-exceptions


## String types

- https://haskell-lang.org/tutorial/string-types


## Laziness

- https://en.wikibooks.org/wiki/Haskell/Laziness


## Free Monads

- http://www.parsonsmatt.org/2017/09/22/what_does_free_buy_us.html
- http://www.haskellforall.com/2012/06/you-could-have-invented-free-monads.html
- https://markkarpov.com/post/free-monad-considered-harmful.html


## Haskell Libraries

- http://www.serpentine.com/wreq/ (easy HTTP requests)
- http://hspec.github.io/  (Very good official docs: `Hspec: A Testing Framework for Haskell`)
- https://jaspervdj.be/hakyll/tutorials/05-snapshots-feeds.html (Hakyll - how to generate RSS/Atom feed)
- https://stackoverflow.com/questions/22620622/listen-on-specific-host-using-warp (Listen to localhost 127.0.0.1 using warp server)
- http://haskelliseasy.readthedocs.io/en/latest/


## Parsing

- https://www.safaribooksonline.com/blog/2015/03/30/high-performance-log-parsing-in-haskell-part-one/
- https://www.safaribooksonline.com/blog/2015/12/14/high-performance-log-parsing-in-haskell-part-two/


## JSON parsing

- https://haskell-lang.org/library/aeson
- https://artyom.me/aeson (Could be the best tutorial on Aeson)


## Haskell Papers

- http://www.staff.city.ac.uk/~ross/papers/Applicative.pdf (Functional Pearl: Applicative programming with effects. Conor McBride, Ross Paterson)
- https://www.andres-loeh.de/Servant/servant-wgp.pdf (Type-level Web APIs with Servant)


## Haskell in industry

- https://news.ycombinator.com/item?id=15931504 (Anatomy of a Haskell-based Application, Revisited)
- https://code.facebook.com/posts/302060973291128/open-sourcing-haxl-a-library-for-haskell/


## Real world projects

- https://ocharles.org.uk/blog/posts/2013-07-26-a-comparison-between-perl-and-haskell.html


## Haskell advertisement

- https://engineering.imvu.com/2014/03/24/what-its-like-to-use-haskell/
- https://www.quora.com/What-do-you-use-Haskell-for-at-work/answer/Tikhon-Jelvis
- https://www.snoyman.com/blog/2017/12/what-makes-haskell-unique (very good)
- https://www.quora.com/What-do-C-C++-programmers-know-that-Haskell-programmers-dont/answer/Costya-Perepelitsa
- https://www.quora.com/What-are-some-use-cases-for-which-it-would-be-beneficial-to-use-Haskell-rather-than-R-or-Python-in-data-science/answer/Tikhon-Jelvis
- https://www.quora.com/What-are-the-advantages-of-Haskell-over-other-functional-programming-languages/answer/Jesse-Tov
- https://www.quora.com/What-are-the-practical-application-areas-for-functional-programming-in-the-real-world/answer/Jacqueline-Homan
- https://www.quora.com/Can-an-operating-system-be-written-in-a-functional-language/answer/Simon-Marlow-2
- https://www.quora.com/Which-of-Haskell-and-OCaml-is-more-practical-For-example-in-which-aspect-will-each-play-a-key-role/answer/Tikhon-Jelvis


## General Advice (not entirely Beginner)

- https://www.quora.com/What-is-the-track-to-mastering-Haskell-and-where-would-it-lead-me-professionally/answer/Edward-Kmett
- https://www.quora.com/What-is-your-review-of-Haskell-programming-language/answer/Edward-Kmett
- https://www.quora.com/How-can-I-avoid-writing-obfuscated-Haskell-code/answer/Tikhon-Jelvis
- https://www.reddit.com/r/haskell/comments/82izs7/what_type_of_projects_can_i_complete_in_haskell/dvajrq4/


## Syntax

- https://haskell-lang.org/tutorial/operators


## IDEs

- https://github.com/rainbyte/haskell-ide-chart


## Debuggers

- https://github.com/ndmitchell/debug


## What can be done better

- https://www.reddit.com/r/haskelltil/comments/337g7t/endo_the_monoid_of_endomorphisms_under/


## Algorithms

- https://futtetennismo.me/posts/algorithms-and-data-structures/2017-12-08-functional-graphs.html


## Type Operators

- https://ocharles.org.uk/blog/posts/2014-12-08-type-operators.html
- https://github.com/ocharles/blog/blob/master/code/2014-12-08-type-operators.hs


## Overlapping Instances

- https://ghc.haskell.org/trac/ghc/wiki/SafeHaskell/NewOverlappingInstances
- https://kseo.github.io/posts/2017-02-05-avoid-overlapping-instances-with-closed-type-families.html


## Orphan Instance

- https://wiki.haskell.org/Orphan_instance


## Multiple Instances

- https://wiki.haskell.org/Multiple_instances


## Preludes

- http://www.stephendiehl.com/posts/protolude.html


## seq combinator

- https://stackoverflow.com/a/12620418 (All monads in Haskell are only monads if we exclude `seq` combinator)


## Covariance and contravariance

- https://www.reddit.com/r/haskell/comments/5c0g5n/covariance_and_contravariance_fp_complete/d9u6d8v/


## Reification

- https://stackoverflow.com/a/5316014 (What do "reify" ad "reification" mean in the context of functional programming?)


## Type level programming

- https://kseo.github.io/posts/2017-01-16-type-level-functions-using-closed-type-families.html
- https://kseo.github.io/posts/2017-01-30-type-level-insertion-sort.html


## Language extensions

- https://ocharles.org.uk/blog/posts/2014-12-05-bang-patterns.html
- https://ocharles.org.uk/blog/posts/2014-12-04-record-wildcards.html
- https://ocharles.org.uk/blog/guest-posts/2014-12-15-deriving.html
- https://ocharles.org.uk/blog/posts/2014-12-12-type-families.html
- https://kseo.github.io/posts/2017-01-08-visible-type-application-ghc8.html (TypeApplications)
- http://www.haskellforall.com/2015/10/polymorphism-for-dummies.html (TypeApplications)

## Testing

- http://willsewell.com/posts/2016-01-23-unit-test-io-in-haskell.html


## Equational Reasoning

- http://www.haskellforall.com/2013/12/equational-reasoning.html


## Strange language features

- http://www.haskellforall.com/2018/02/the-wizard-monoid.html


## Finance

- https://twitter.com/k0001/status/942196512544706560 (safe-money: Money in the type system where it belongs)


## Learning Journies

- https://kseo.github.io/posts/2017-01-27-how-i-learned-haskell.html


## Haskell blogs

- https://kseo.github.io/
- http://abailly.github.io/
- https://teh.id.au/index.html


## Appreciation

- https://www.quora.com/Is-it-pleasant-for-you-to-read-and-maintain-other-peoples-Haskell-code/answer/Tikhon-Jelvis


## Humor

- https://twitter.com/fylwind/status/549342595940237312?lang=en
- https://twitter.com/codemiller/status/695516883483828224?s=03
- https://twitter.com/Functionalworks/status/680774070418423809?s=03 (FP languages as Star Wars characters)
- https://artyom.me/haskell-10 (10 questions about Haskell: a shitpost)


## Other resources

- https://stackoverflow.com/a/16951/732396 (Beginners Guide to Haskell?)
- https://stackoverflow.com/a/1016986/732396
- https://userpages.uni-koblenz.de/~laemmel/paradigms1011/resources/pdf/haskell.pdf (There are other resources on the site)
- https://www.fpcomplete.com/haskell-syllabus (can look at this and create a custom syllabus of sorts)
- https://stackoverflow.com/questions/6398996/good-haskell-source-to-read-and-learn-from
