## gdb

- https://stackoverflow.com/a/5429148 (print register values in gdb)
- http://www.os4coding.net/forum/using-gdb-do-hex-dump-memory (hexdump of memory)
- https://stackoverflow.com/q/1902901 (show current instructions)
- https://stackoverflow.com/a/15629298 (supply stdin to a running program)
- https://stackoverflow.com/a/5459664 (set breakpoint on a memory address)

## Tools

- https://onlinedisassembler.com/odaweb/
