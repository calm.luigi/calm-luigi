- https://news.ycombinator.com/item?id=22800136 (What is your blog and why should I read it?)
- https://ncase.me/remember/
- https://news.ycombinator.com/item?id=22050802 (Thank HN: You helped me get a new job)
- https://news.ycombinator.com/item?id=22097373 (Ask HN: Do you ever contact people who have had a positive impact on you?)
- https://news.ycombinator.com/item?id=22096628 (Ask HN: Are compiler engineers still in demand, and what do they work on?)
- https://news.ycombinator.com/item?id=22096359 (Analysis of compensation, level, and experience details of 19k tech workers)
- https://news.ycombinator.com/item?id=22087419 (Lisping at JPL 2002)
- https://news.ycombinator.com/item?id=22105229 (Tricks to start working despite not feeling like it)
- https://news.ycombinator.com/item?id=22111385 (Stack Overflow is forming a moderator council)
- https://news.ycombinator.com/item?id=22098425 (Thank HN: My SaaS paid my rent this month)
- https://news.ycombinator.com/item?id=22101066 (My story as a self-taught AI researcher)
- https://news.ycombinator.com/item?id=21762640 (Learning at work is work, and we must make space for it)
- https://news.ycombinator.com/item?id=21882441 (Takeaways from coaching CEOs, founders and VCs)
- https://news.ycombinator.com/item?id=21861412 (Ask HN: Haven't worked for a while, best guide/advice to start a hobby project?)
- https://news.ycombinator.com/item?id=21855986 (Shown HN: Local Node.js app to save everything you browse and serve it offline)
- https://news.ycombinator.com/item?id=22031009 (Being a Solo Founder: Pros, Cons, Tips and Tricks)
- https://threadreaderapp.com/thread/1214274038933020672.html
- https://news.ycombinator.com/item?id=20179203 (How Nike sold its first shoes)
- https://news.ycombinator.com/item?id=22382536 (Requirements volatility is the core problem of software engineering)
- https://news.ycombinator.com/item?id=18401481 (How do you keep track of the articles you want to read?)
- https://news.ycombinator.com/item?id=20477104 (Ask HN: What have the past 12 months taught you?)
- https://betterhumans.coach.me/how-to-chart-a-new-course-for-your-life-with-3-simple-diagrams-e9cc6b59c49d
- https://sites.google.com/site/jerrylinxp/%E7%BD%97%E9%A9%AC%E4%BA%BA%E7%9A%84%E6%95%85%E4%BA%8B42222
- https://news.ycombinator.com/item?id=22836883 (Untangling microservices, or balancing complexity in distributed systems)
- https://news.ycombinator.com/item?id=22826722 (Recommend me a course on coursera)
- https://news.ycombinator.com/item?id=22818896 (I graduated into the dot com bust as a programmer and made it - you will too)
- https://news.ycombinator.com/item?id=23072333 (Extremely disillusioned with technology, please help)
- https://www.amazon.com/Thinking-Bets-Making-Smarter-Decisions-ebook/dp/B074DG9LQF/ref=sr_1_1?dchild=1&keywords=thinking+bets+making+smarter+decisions&link_code=qs&qid=1589937358&sourceid=Mozilla-search&sr=8-1 (Thinking in Bets: Making smarter decisions when you don't have all the facts)


## Big Tech

- https://bradfitz.com/2020/01/27/leaving-google
- https://news.ycombinator.com/item?id=21645117 (Ask HN: Quitting Big Tech, what is it like)


## Burnout

- https://news.ycombinator.com/item?id=21661054 (Ask HN: Burning Out)


## Snake oil

- https://www.reddit.com/r/MachineLearning/comments/eyg2hv/d_does_actual_knowledge_even_matter_in_the_real/
- https://www.cs.princeton.edu/~arvindn/talks/MIT-STS-AI-snakeoil.pdf
- https://www.reddit.com/r/MachineLearning/comments/donbz7/d_im_so_sick_of_the_hype/


## Books

- http://a.co/gzncekw (Getting Things Done: The Art of Stress-Free Productivity [David Allen, James Fallows])
- http://a.co/885xxkp (All Marketers are Liars: The Underground Classic That Explains How Marketing Really Works -- and Why Authenticity is the Best Marketing of All [Seth Godin])
- http://a.co/096h1Je (The Now Habit: A Strategic Program for Overcoming Procrastination and Enjoy Guilt-Free Play [Neil Fiore])
- https://www.principles.com/ (Principes by Ray Dalio)
- http://a.co/8q1t7qL (Monster Loyalty: How Lady Gaga Turns Followers into Fanatics [Jackie Huba])
- http://a.co/d/54q9yOd (How To Sell Your Way Through Life [Napoleon Hill])
- http://a.co/d/h1Ddgh0 (Looking Out for Number One: How to Get from Where You Are Now to Where You Want to Be in Life [Robert Ringer])
- http://a.co/d/aAjzfLm (Who Rules the World? [Noam Chomksy])
- https://www.amazon.com/War-Art-Through-Creative-Battles/dp/1936891026 (The War of Art: Break Through the Blocks and Win Your Inner Creative Battles [Steven Pressfield])
- https://www.amazon.com/Maximum-Achievement-Strategies-Skills-Succeed/dp/0684803313/ (Maximum Achievement: Strategies and Skills That Will Unlock Your Hidden Power to Succeed [Brian Tracy])
- https://www.amazon.com/gp/product/0143126563/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=0143126563&linkCode=as2&tag=ucmbread-20&linkId=RC6A6XTCRWTZFSJE (Getting Things Done: The Art of Stress-Free Productivity [David Allen, James Fallows])
