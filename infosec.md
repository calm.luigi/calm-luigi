# About

- https://github.com/nccgroup/ScoutSuite
- https://guif.re/windowseop
- https://medium.com/bugbountywriteup/cvv-1-local-file-inclusion-ebc48e0e479a
- http://pwnwiki.io/#!index.md
- https://www.corelan.be/index.php/2009/07/19/exploit-writing-tutorial-part-1-stack-based-overflows/
- https://artkond.com/2017/03/23/pivoting-guide/
- https://www.offensive-security.com/metasploit-unleashed/client-side-exploits/
- https://isc.sans.edu/diary/Windows+Command-Line+Kung+Fu+with+WMIC/1229
- https://github.com/justinsteven/dostackbufferoverflowgood
- https://blog.g0tmi1k.com/2011/03/sickfuzz-v02/
- https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA (HackTheBox video walkthroughs)
- http://repository.root-me.org/Administration/Unix/EN%20-%20sudo%20:%20you're%20doing%20it%20wrong.pdf
- https://www.samba.org/samba/docs/using_samba/ch07.html
- https://news.ycombinator.com/item?id=17563857 (Best resources for learning security and pen testing)
- https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/
- https://www.codevoila.com/post/16/convert-socks-proxy-to-http-proxy-using-polipo
- https://github.com/emilyanncr/Windows-Post-Exploitation
- https://github.com/netbiosX/Checklists/blob/master/Windows-Privilege-Escalation.md
- https://support.microsoft.com/en-sg/help/947709/how-to-use-the-netsh-advfirewall-firewall-context-instead-of-the-netsh
- https://en.wikipedia.org/wiki/Code_injection#Shell_injection
- https://medium.com/@6c2e6e2e/spawning-interactive-reverse-shells-with-tty-a7e50c44940e
- https://www.pressreader.com/australia/linux-format/20180731/281706910474220
- https://speakerdeck.com/knaps/escape-from-shellcatraz-breaking-out-of-restricted-unix-shells
- https://amonsec.net/training/windows-exploit-development/2018/windows-stack-based-buffer-overflow
- https://www.hypn.za.net/blog/2018/05/27/thoughts-on-offensive-securitys-oscp-certification-in-2018-pre-exam/
- https://www.hacksplaining.com/
- https://websec.ca/kb/sql_injection#MySQL_Default_Databases
- https://www.win.tue.nl/~aeb/linux/hh/netcat_tutorial.pdf
- https://gironsec.com/papers/Static%20PHP%20Sauce%20Audit.pdf
- https://gironsec.com/papers/Web-Shells-rev2.doc
- https://www.reddit.com/r/netsecstudents/
- https://www.reddit.com/r/HowToHack/
- https://github.com/xxooxxooxx/xxooxxooxx.github.io/wiki/OSCP-Survival-Guide
- https://medium.com/@hakluke/sensitive-files-to-grab-in-windows-4b8f0a655f40
- https://www.christophertruncer.com/i-have-the-password-hashes-can-i-pass-them/
- https://www.offensive-security.com/metasploit-unleashed/psexec-pass-hash/
- https://www.exploit-db.com/papers/13045
- https://alexandreborgesbrazil.files.wordpress.com/2013/08/introduction_to_password_cracking_part_1.pdf
- https://chamibuddhika.wordpress.com/2012/03/21/ssh-tunnelling-explained/
- https://crap.sics.se/faq?faqID=98
- https://www.itprotoday.com/compute-engines/how-can-i-configure-services-start-type-command-line
- https://stackoverflow.com/a/31165218
- https://superuser.com/questions/322423/explain-the-output-of-icacls-exe-line-by-line-item-by-item
- https://www.pointdev.com/en/faq/faq-ideal-administration-send-ctrl-alt-del-tightvnc-vnc-logon-sas-computer-machine-id-227.html
- https://serverfault.com/questions/490841/how-to-display-the-first-n-lines-of-a-command-output-in-windows-the-equivalent/716619#716619
- https://www.gnucitizen.org/blog/coldfusion-directory-traversal-faq-cve-2010-2861/
- http://hacking-share.blogspot.com/2013/03/hacking-cold-fusion-servers-part-i.html
- http://vinc.top/2017/04/19/mysql-udf%E6%8F%90%E6%9D%83linux%E5%B9%B3%E5%8F%B0/
- https://medium.com/@the.bilal.rizwan/wordpress-xmlrpc-php-common-vulnerabilites-how-to-exploit-them-d8d3c8600b32
- https://blog.sucuri.net/2015/10/brute-force-amplification-attacks-against-wordpress-xmlrpc.html
- https://testpurposes.net/2016/11/01/wordpress-xmlrpc-brute-force-attacks-via-burpsuite/
- https://www.cubettech.com/blog/brute-force-amplification-attacks-against-wp-xmlrpc/
- https://netsec.ws/?p=262
- https://netsec.ws/?p=287
- https://www.codevoila.com/post/16/convert-socks-proxy-to-http-proxy-using-polipo
- https://myexploit.wordpress.com/control-keimpx-py-pass-the-hash/
- https://github.com/SecWiki/windows-kernel-exploits/blob/master/README.md
- https://medium.com/@airman604/installing-impacket-on-windows-ded7ba8bec9a
- https://stackoverflow.com/questions/2950971/cross-compiling-a-python-script-on-linux-into-a-windows-executable
- https://forums.offensive-security.com/showthread.php?11629-Good-URL-for-accesschk
- http://www.hackingarticles.in/editing-etc-passwd-file-for-privilege-escalation/
- https://www.cybrary.it/ (various free courses, eg. Advanced Penetration Testing)
- https://github.com/togie6/Windows-Privesc
- https://github.com/abatchy17/WindowsExploits
- https://github.com/lucyoa/kernel-exploits
- https://github.com/PowerShellMafia/PowerSploit/tree/master/Privesc
- https://github.com/toniblyx/my-arsenal-of-aws-security-tools
- https://notsosecure.com/exploiting-ssrf-in-aws-elastic-beanstalk
- https://twitter.com/mhausenblas/status/1144354232579108871?s=03 (Vulnerable containers)
- https://www.youtube.com/watch?v=yaLq88OjfoQ (Blue Team Security Actual Security work for Actual Developers by Siren Hofvander)
- https://www.digitalocean.com/community/tutorials/how-to-audit-docker-host-security-with-docker-bench-for-security-on-ubuntu-16-04
- https://capsule8.com/blog/practical-container-escape-exercise/
- https://news.ycombinator.com/item?id=21480777 (Security assessment techniques for Go projects)
- https://gist.github.com/Belphemur/82d27b1b6dfd675d15f2
- https://www.cyberark.com/threat-research-blog/the-route-to-root-container-escape-using-kernel-exploitation/
- https://www.dwheeler.com/essays/fixing-unix-linux-filenames.html
- https://www.dwheeler.com/essays/filenames-in-shell.html
- https://medium.com/google-cloud/automating-container-security-on-gke-with-binary-authorization-and-circleci-7efa7a6be2a7
- https://www.gracefulsecurity.com/privesc-stealing-windows-access-tokens-incognito/
- https://www.gracefulsecurity.com/web-app/
- https://www.gracefulsecurity.com/sql-injection-cheat-sheet-mssql/
- https://www.gracefulsecurity.com/burp-suite-vs-csrf-tokens/
- https://www.slideshare.net/isnuryusuf/devops-indonesia-devsecops-the-open-source-way (some Docker security tools)
- https://blog.cobaltstrike.com/2015/05/21/how-to-pass-the-hash-with-mimikatz/
- https://attack.mitre.org
- https://www.slideshare.net/billkarwin/sql-injection-myths-and-fallacies (READ AGAIN)
- https://www.owasp.org/index.php/Data_Validation
- https://firejail.wordpress.com/
- https://wiki.archlinux.org/index.php/Firejail
- https://www.owasp.org/index.php/Category:OWASP_WebGoat_Project
- https://www.troyhunt.com/how-to-build-and-how-not-to-build/  (How to build and how not to build a secure "remember me" feature)
- https://news.ycombinator.com/item?id=16017969 (No boundaries for user identities: Web trackers exploit browser login managers)
- https://news.ycombinator.com/item?id=14321498 (Ask HN: How do you manage SSH keys and SSL certificates in your company?)
- http://www.catonmat.net/blog/ldd-arbitrary-code-execution/
- https://www.owasp.org/index.php/Category:Control
- https://www.owasp.org/images/7/72/OWASP_Top_10-2017_%28en%29.pdf.pdf
- https://www.bleepingcomputer.com/news/security/amazon-aws-servers-might-soon-be-held-for-ransom-similar-to-mongodb/
- https://www.owasp.org/index.php/REST_Assessment_Cheat_Sheet
- https://www.owasp.org/index.php/REST_Security_Cheat_Sheet
- https://www.owasp.org/index.php/OWASP_Secure_Headers_Project
- https://www.owasp.org/images/2/29/OWASPSpain8_On-breaking-php-based-cross-site-scripting-protections-in-the-wild.pdf
- http://seclab.stanford.edu/websec/csrf/csrf.pdf
- https://www.reddit.com/r/xss/comments/736wv8/where_to_start_with_xss/
- https://scarybeastsecurity.blogspot.sg/2009/12/generic-cross-browser-cross-domain.html
- https://www.reddit.com/r/webdev/comments/87smdo/as_a_full_stack_web_developer_ive_recently_taken/?st=JFC3OLDT&sh=91daa7ed
- https://medium.com/@jeremy.trinka/7-resources-to-get-you-started-in-operational-cybersecurity-a53e374fded9
- https://blog.vonhewitt.com/2018/08/oscp-exam-cram-log-aug-sept-oct-2018/
- https://github.com/forter/security-101-for-saas-startups
- https://github.com/teesloane/Auth-Boss
- https://www.digitalocean.com/community/tutorials/what-is-a-firewall-and-how-does-it-work
- https://security.stackexchange.com/questions/22711/is-it-a-bad-idea-for-a-firewall-to-block-icmp/22713#22713
- https://serverfault.com/questions/84963/why-not-block-icmp/84981#84981
- http://www.chiark.greenend.org.uk/~peterb/network/drop-vs-reject
- http://shouldiblockicmp.com/
- https://www.quora.com/What-is-ICMP-Why-should-you-block-it
- https://blog.cloudflare.com/staying-on-top-of-tls-attacks/
- https://blog.serverdensity.com/how-to-secure-your-webapp/
- http://security.blogoverflow.com/2013/09/about-secure-password-hashing/
- https://www.offensive-security.com/kali-linux/kali-rolling-iso-of-doom/ (Unattended Kali Linux installation that will use an OpenVPN bridge connection to connect back to an OpenVPN server and enable internal network access to clients connected to the OpenVPN server)
- https://hackernoon.com/how-i-hacked-40-websites-in-7-minutes-5b4c28bc8824
- https://medium.com/@rosshosman/1password-sends-your-password-across-the-loopback-interface-in-clear-text-307cefca6389
- https://hackernoon.com/10-things-infosec-professionals-need-to-know-about-networking-d159946efc93
- https://www.business2community.com/cybersecurity/turn-cyber-kill-chain-attacker-01398794/amp
- https://www.digitalocean.com/community/tutorials/7-security-measures-to-protect-your-servers
- https://www.digitalocean.com/community/tutorials/how-to-use-the-linux-auditing-system-on-centos-7


## Wireshark

- http://biot.com/capstats/bpf.html
- https://superuser.com/a/543842 (Wireshark filter for TLS SNI)


## AWS security

- https://github.com/Netflix/security_monkey
- https://github.com/nccgroup/Scout2
- https://aws.amazon.com/waf/
- https://aws.amazon.com/inspector/
- https://github.com/andresriancho/nimbostratus (fingerprinting and exploiting AWS cloud infra)


## Bash / shell vulnerabilities

- https://news.ycombinator.com/item?id=17376895 (UNIX Wildcards Gone Wild)


## Docker

- https://dev.to/heroku/a-house-of-cards-an-exploration-of-security-when-building-docker-containers--33in
- https://docs.docker.com/engine/security/userns-remap/


## nmap

https://nmap.org/book/nse-usage.html


## Metasploit

- https://security.stackexchange.com/a/169840


## Running untrusted code

- https://news.ycombinator.com/item?id=11532599


## Certificate Transparency

- https://scotthelme.co.uk/certificate-transparency-an-introduction/


## Side Channel Attacks

- https://www.nccgroup.trust/us/about-us/newsroom-and-events/blog/2018/june/its-back...understanding-the-return-of-the-hidden-number-problem/


## BGP security

- https://dyn.com/blog/pakistan-hijacks-youtube-1/
- https://web.archive.org/web/20180427153929/https://arstechnica.com/information-technology/2018/04/suspicious-event-hijacks-amazon-traffic-for-2-hours-steals-cryptocurrency/


## SQLMap

- https://security.stackexchange.com/a/6084 (How to specify injection points in URL path components)
- https://pen-testing.sans.org/blog/2017/10/13/sqlmap-tamper-scripts-for-the-win
- https://forum.bugcrowd.com/t/sqlmap-tamper-scripts-sql-injection-and-waf-bypass/423 (Use multiple tamper scripts in same run)
- https://github.com/sqlmapproject/sqlmap/tree/master/tamper (list of tamper scripts)


## SQL Injection

- http://securityidiots.com/Web-Pentest/SQL-Injection/Blind-SQL-Injection.html
- https://www.gracefulsecurity.com/sql-injection-exploitation/


## Other types of injection

- https://news.ycombinator.com/item?id=17358103 (YAML injections; YAML: probably not so great after all)


## XXE (XML External Entities)

- https://www.blackhillsinfosec.com/xml-external-entity-beyond-etcpasswd-fun-profit/
- https://depthsecurity.com/blog/exploitation-xml-external-entity-xxe-injection
- https://www.bugcrowd.com/advice-from-a-bug-hunter-xxe/


## Insecure Direct Object Reference

- https://www.owasp.org/index.php/Top_10_2013-A4-Insecure_Direct_Object_References
- https://cwe.mitre.org/data/definitions/639.html


## CSRF

https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html


## ZAP / Zed Attack Proxy

- https://www.youtube.com/watch?v=o_JZRgQMF4Q (Security Testing with OWASP ZAP in CI/CD - Simon Bennetts - Codemotion Amsterdam 2017)
- https://www.youtube.com/watch?v=F5CyM4akOAs (ZAP Tutorial - Ajax Spidering authenticated websites)


## Vulnerability Scanners

- https://vulners.com/landing (not exactly a vulnerability scanner. But detects potential vulnerabilities based on a list of software packages installed)


## Fuzzing

- https://alexgaynor.net/2019/feb/05/notes-fuzzing-imagemagick-graphicsmagick/
- https://github.com/google/oss-fuzz
- https://nullprogram.com/blog/2019/01/25/


## Malicious Traffic Generator

- https://cybertestsystems.com/#!/products


## Inspirational

- https://www.youtube.com/watch?v=byJBR16xUnc (Gone in 60ms: offensive security in the serverless age)


## Blogs

- https://aws.amazon.com/blogs/security/
- https://alex.kaskaso.li/
- https://blog.mozilla.org/security/


## Books

- http://a.co/d/2jJh0T1 (The Hacker Playbook 3: Practical Guide to Penetration Testing)


## Opinion based articles

- https://alexgaynor.net/2018/jul/20/worst-truism-in-infosec/


## Interviews

- http://resources.infosecinstitute.com/top-50-information-security-interview-questions/
- https://danielmiessler.com/study/infosec_interview_questions/
- https://www.simplilearn.com/cyber-security-interview-questions-and-answers-article
- https://www.synopsys.com/blogs/software-security/web-appsec-interview-questions/
