## Compilers

- https://www.youtube.com/playlist?list=PLmV5I2fxaiCKfxMBrNsU1kgKJXD3PkyxO (Jonathan Blow - A Programming Language for Games)
- https://www.cl.cam.ac.uk/teaching/2005/OptComp/slides/
- https://www.amazon.com/Trustworthy-Compilers-Vladimir-Safonov/dp/0470500956 (Trustworthy Compilers [by Vladimir O. Safonov])
- https://news.ycombinator.com/item?id=19728749 (Introduction to Compilers and Language Design)
- https://www.quora.com/What-is-the-future-of-optimizing-compilers/answer/Tikhon-Jelvis?share=facf7cee
- https://www.quora.com/Can-a-functional-programming-language-compiler-optimize-as-well-as-an-imperative-programming-language-compiler-in-practice/answer/Tikhon-Jelvis
- http://elm-lang.org/blog/compiler-errors-for-humans
- https://www.reddit.com/r/askscience/comments/9wex4k/didnt_the_person_who_wrote_worlds_first_compiler/
- https://www.reddit.com/r/askscience/comments/9wex4k/didnt_the_person_who_wrote_worlds_first_compiler/e9kg0o5/ (Good explanation of linkers)
- http://steve-yegge.blogspot.com/2007/06/rich-programmer-food.html
- https://www.reddit.com/r/compsci/comments/g70tm9/learning_about_compilers/


## Programming Languages

- https://papl.cs.brown.edu/2018/
- http://cs.brown.edu/~sk/Publications/Books/ProgLangs/ (Programming Language: Application and Interpretation by Shiram Krishnamurthi)
- http://www.cs.rochester.edu/~scott/pragmatics/ (Programming Language Pragmatics)
- https://blog.acolyer.org/2018/01/26/a-practitioners-guide-to-reading-programming-languages-papers/
- http://siek.blogspot.sg/2012/07/crash-course-on-notation-in-programming.html
- https://yager.io/programming/go.html
- https://www.quora.com/What-are-criticisms-of-the-Go-language/answer/Tikhon-Jelvis
- https://www.quora.com/Do-you-agree-with-John-Backus-in-Can-Programming-Be-Liberated-from-the-Von-Neumann-Style-regarding-functional-style-programming/answer/Tikhon-Jelvis
- https://www.quora.com/What-are-some-differences-between-the-many-functional-programming-languages-What-makes-one-more-suitable-for-a-given-requirement/answer/Jesse-Tov
- https://www.quora.com/What-are-some-myths-about-functional-programming-and-functional-programming-languages/answer/Tikhon-Jelvis
- https://www.quora.com/Why-doesnt-every-programming-language-have-type-inference/answer/Tikhon-Jelvis
- https://twitter.com/tikhonjelvis/status/983192976405901317?s=03 (Great overview of modern GC technology)
- https://github.com/typelead/eta (Haskell on the JVM)


## Linkers

- https://twitter.com/b0rk/status/986804867170742277?s=03 (Julia Evans E-zine: shared libraries and dynamic linking)
