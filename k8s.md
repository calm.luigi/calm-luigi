- https://news.ycombinator.com/item?id=17522020 (Implementing Open Source Security, Part 1: Runtime Security)
- https://news.ycombinator.com/item?id=18953647 (Kubernetes Failure Stories)
- https://github.com/aquasecurity/kube-bench
- https://sysdig.com/blog/selinux-seccomp-falco-technical-discussion/
- https://twitter.com/nathankpeck/status/1158783728476463105?s=09 (New k8s patch that fixes 2 CVEs)
- https://thenewstack.io/no-more-forever-tokens-changes-in-identity-management-for-kubernetes/


## Security

- https://www.cyberark.com/threat-research-blog/securing-kubernetes-clusters-by-eliminating-risky-permissions/
- https://www.cyberark.com/threat-research-blog/kubernetes-pentest-methodology-part-1/
- https://github.com/kubernetes/kubernetes/issues/4957 (Secrets in a namespace are available to anyone who can create Pods in that namespace)
- https://github.com/kubernetes/community/pull/1604 (Discussion that shows security isolation in k8s happens at namespace level)
- https://docs.aws.amazon.com/eks/latest/userguide/restrict-ec2-credential-access.html

### Exploits

- https://kubernetes.io/blog/2019/02/11/runc-and-cve-2019-5736/
- https://capsule8.com/blog/practical-container-escape-exercise/
- https://www.cyberark.com/threat-research-blog/the-route-to-root-container-escape-using-kernel-exploitation/
- https://labs.mwrinfosecurity.com/blog/attacking-kubernetes-through-kubelet/
